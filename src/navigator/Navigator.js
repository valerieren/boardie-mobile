import React from 'react';
import {Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import RootStackScreen from './stacks/RootStackScreen';

const Navigator = () => {
  return (
    <NavigationContainer fallback={<Text>Loading...</Text>}>
      <RootStackScreen />
    </NavigationContainer>
  );
};

export default Navigator;
