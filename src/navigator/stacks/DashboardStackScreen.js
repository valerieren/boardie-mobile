import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import DashboardScreen from '../../app/dashboard/components';
import NewGamesScreen from '../../app/dashboard/components/new-games';

const DashboardStack = createStackNavigator();

const DashboardStackScreen = () => (
  <DashboardStack.Navigator
    headerMode="none"
    screenOptions={{
      headerShown: false,
    }}>
    <DashboardStack.Screen name="DashboardScreen" component={DashboardScreen} />
    <DashboardStack.Screen name="NewGamesScreen" component={NewGamesScreen} />
  </DashboardStack.Navigator>
);

export default DashboardStackScreen;
