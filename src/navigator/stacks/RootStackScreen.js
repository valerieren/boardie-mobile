import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import DashboardStackScreen from './DashboardStackScreen';
import GameStackScreen from './GameStackScreen';

const RootStack = createStackNavigator();

const RootStackScreen = () => (
  <RootStack.Navigator
    headerMode="none"
    screenOptions={{
      headerShown: false,
    }}>
    <RootStack.Screen name="Dashboard" component={DashboardStackScreen} />
    <RootStack.Screen name="Game" component={GameStackScreen} />
  </RootStack.Navigator>
);

export default RootStackScreen;
