import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import BlackJackScreen from '../../app/games/components/black-jack/BlackJackScreen';

const GameStack = createStackNavigator();

const GameStackScreen = () => (
  <GameStack.Navigator
    headerMode="none"
    screenOptions={{
      headerShown: false,
    }}>
    <GameStack.Screen name="BlackJackScreen" component={BlackJackScreen} />
  </GameStack.Navigator>
);

export default GameStackScreen;
