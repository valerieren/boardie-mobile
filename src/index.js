import React from 'react';
import Navigator from './navigator/Navigator';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import {createStore, applyMiddleware} from 'redux';
import rootReducer from './app/reducer';
import WebSocketProvider from './utils/widgets/Websocket';

const store = createStore(rootReducer, applyMiddleware(thunk));

const Bordie = () => (
  <Provider store={store}>
    <WebSocketProvider>
      <Navigator />
    </WebSocketProvider>
  </Provider>
);

export default Bordie;
