const initialState = {
  allGames: [],
};

// reducer
export default function gamesReducer(state = initialState, action = {}) {
  switch (action.type) {
    case 'ADD_ALL_GAMES_SUCCESS':
      return {
        ...state,
        allGames: action.payload,
      };
    default:
      return state;
  }
}
