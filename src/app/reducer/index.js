import {combineReducers} from 'redux';
import gamesReducer from './games';
import playersReducer from './players';
import logReducer from './log';

export default combineReducers({
  games: gamesReducer,
  players: playersReducer,
  logs: logReducer,
});
