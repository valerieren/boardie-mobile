import {UPDATE_WS_LOG} from '../action/websocket';

const initialState = {
  logs: [],
};

// reducer
export default function logReducer(state = initialState, action = {}) {
  switch (action.type) {
    case UPDATE_WS_LOG:
      return {
        logs: action.payload,
      };
    default:
      return state;
  }
}
