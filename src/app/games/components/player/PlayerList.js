import React from 'react';
import {View, Text} from 'react-native';

const PlayerList = (props) => {
  return (
    <View style={{flexDirection: 'column'}}>
      {props.players ? <Text>{props.players.name}</Text> : null}
    </View>
  );
};

export default PlayerList;
