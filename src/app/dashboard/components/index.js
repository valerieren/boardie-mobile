import React from 'react';
import PrimaryButton from '../../../utils/widgets/buttons/PrimaryButton';
import styles from './styles';
import {
  NEW_GAMES,
  ONGOING_GAMES,
  SETTINGS,
} from '../../../utils/constants/dashboard';
import ScreenContainer from '../../../utils/widgets/ScreenContainer';

const DashboardScreen = ({route, navigation}) => {
  return (
    <ScreenContainer style={styles.container}>
      <PrimaryButton
        onPress={() => {
          navigation.navigate('Dashboard', {
            screen: 'NewGamesScreen',
          });
        }}
        text={NEW_GAMES}
      />
      <PrimaryButton
        onPress={() => console.log(ONGOING_GAMES)}
        text={ONGOING_GAMES}
      />
      <PrimaryButton onPress={() => console.log(SETTINGS)} text={SETTINGS} />
    </ScreenContainer>
  );
};

export default DashboardScreen;
