import React, {useEffect, useContext} from 'react';
import styles from '../styles';
import ScreenContainer from '../../../../utils/widgets/ScreenContainer';
import PrimaryButton from '../../../../utils/widgets/buttons/PrimaryButton';
import {
  BLACK_JACK,
  COIN_TOSS,
  PRISONERS_DILEMMA,
  ROCK_PAPER_SCISSORS,
  UNO,
} from '../../../../utils/constants/dashboard-new-games';
import {useDispatch, useSelector} from 'react-redux';
import {getAllGames} from '../../../action/games';
import {WebSocketContext} from '../../../../utils/widgets/Websocket';

const allGames = [
  {name: BLACK_JACK, screen: 'BlackJackScreen'},
  {name: COIN_TOSS, screen: 'CoinTossScreen'},
  {name: PRISONERS_DILEMMA, screen: 'PrisonersDilemmaScreen'},
  {name: UNO, screen: 'UnoScreen'},
  {name: ROCK_PAPER_SCISSORS, screen: 'RockPaperScissorsScreen'},
];
const NewGamesScreen = ({route, navigation}) => {
  const dispatch = useDispatch();
  const playerSelector = useSelector((state) => state.players || '');
  const ws = useContext(WebSocketContext);

  const removeSymbolsAndWhiteSpace = (str) =>
    str.replace(/[^a-zA-Z ]/g, '').replace(/ /g, '');

  useEffect(() => {
    (async function getAllGamesList() {
      const allGamesList = await dispatch(getAllGames());
      allGames.map((game) => {
        const gameName = removeSymbolsAndWhiteSpace(game.name).toLowerCase();
        const match = allGamesList.find(
          (g) => removeSymbolsAndWhiteSpace(g.name).toLowerCase() === gameName,
        );
        game.gameDefinition = match;
        return game;
      });
    })();
  }, []);

  const handleSendCreateNewGame = (gameDefinition, playerName) => {
    ws.sendMessage(gameDefinition.name, playerName);
  };

  return (
    <ScreenContainer style={styles.container}>
      {allGames.length > 0
        ? allGames.map((game) => {
            return (
              <PrimaryButton
                key={game.name}
                onPress={() => {
                  handleSendCreateNewGame(
                    game.gameDefinition,
                    playerSelector.name || 'Player',
                  );
                  navigation.navigate('Game', {
                    screen: game.screen,
                  });
                }}
                text={game.name}
              />
            );
          })
        : null}
    </ScreenContainer>
  );
};

export default NewGamesScreen;
