import {API_BASE} from '../../utils/config';

export const getAllGames = () => {
  const url = `${API_BASE}/gameDefinitions`;
  return fetch(url)
    .then((result) => result.json())
    .catch((error) => console.log(error));
};
