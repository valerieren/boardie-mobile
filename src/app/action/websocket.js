export const UPDATE_WS_LOG = 'UPDATE_WS_LOG';

export const saveWebsocket = (ws) => {
  return (dispatch) => {
    dispatch(saveWebsocketSuccess(ws));
    return ws;
  };
};

export const updateLog = (update) => ({
  type: UPDATE_WS_LOG,
  update,
});

const saveWebsocketSuccess = (ws) => ({
  type: 'ADD_WS_SUCCESS',
  payload: ws,
});
