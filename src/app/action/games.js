import * as GamesService from '../services/GamesService';

export const getAllGames = () => {
  return async (dispatch) => {
    try {
      const allGames = await GamesService.getAllGames();
      dispatch(getAllGamesSuccess(allGames));
      return allGames;
    } catch (error) {
      console.log(error);
    }
  };
};

const getAllGamesSuccess = (allGames) => ({
  type: 'ADD_ALL_GAMES_SUCCESS',
  payload: allGames,
});
