export const BLACK_JACK = 'Black Jack';
export const COIN_TOSS = 'Coin Toss';
export const PRISONERS_DILEMMA = "Prisoners' Dilemma";
export const UNO = 'Uno';
export const ROCK_PAPER_SCISSORS = 'Rock Paper Scissors';
