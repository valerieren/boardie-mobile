import {AppBackground, Primary, Secondary, Tertiary, Text} from '../colors';

export const DefaultTheme = {
  dark: false,
  color: {
    primary: Primary,
    secondary: Secondary,
    tertiary: Tertiary,
    background: AppBackground,
    text: Text,
  },
  fontSize: {
    button: {
      fontSize: 14,
    },
  },
};
