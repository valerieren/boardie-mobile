import React from 'react';
import {useSelector} from 'react-redux';
import {View, Text} from 'react-native';

const LogCard = () => {
  const logSelector = useSelector((state) => state.logs || '');
  const logs = logSelector.logs || [];
  return <View>{logs ? logs.map((log) => <Text>{log}</Text>) : null}</View>;
};

export default LogCard;
