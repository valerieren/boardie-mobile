import React, {createContext} from 'react';
import io from 'socket.io-client';
import {WS_BASE} from '../config';
import {useDispatch} from 'react-redux';
import {updateLog} from '../../app/action/websocket';

const WebSocketContext = createContext(null);

export {WebSocketContext};

const Websocket = ({children}) => {
  let socket;
  let ws;

  const dispatch = useDispatch();

  const sendMessage = (gameName, playerName) => {
    const payload = {
      gameName: gameName,
      playerName: playerName,
    };

    socket.emit('newGame', JSON.stringify(payload), (data) => {
      console.log(data);
    });
    dispatch(updateLog(payload));
  };

  if (!socket) {
    socket = io.connect(WS_BASE);

    socket.on('message', (msg) => {
      const payload = JSON.parse(msg);
      dispatch(updateLog(payload));
    });

    ws = {
      socket: socket,
      sendMessage,
    };
  }

  return (
    <WebSocketContext.Provider value={ws}>{children}</WebSocketContext.Provider>
  );
};

export default Websocket;
