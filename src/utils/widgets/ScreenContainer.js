import React from 'react';
import {StyleSheet, View} from 'react-native';
import LogCard from './card/LogCard';

const ScreenContainer = ({children}) => (
  <>
    <View style={styles.container}>{children}</View>
    <LogCard style={{position: 'absolution', top: 50, left: 50}} />
  </>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 100,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});

export default ScreenContainer;
