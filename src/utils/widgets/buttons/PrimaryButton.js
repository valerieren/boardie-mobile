import React from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import {screenDimensions} from '../../dimensions';
import {DefaultTheme} from '../../themes';

const PrimaryButton = (props) => {
  const btnStyle = {
    ...styles.primaryButton,
    ...props.buttonStyle,
    width: props.width ? props.width : screenDimensions.width / 1.3,
    height: props.height ? props.height : 40,
  };

  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={btnStyle}
      activeOpacity={props.disabled}>
      <Text style={{...styles.primaryButtonText, ...props.textStyle}}>
        {props.text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  primaryButton: {
    height: 50,
    borderRadius: 50,
    width: screenDimensions.width / 1.5,
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: DefaultTheme.color.secondary,
  },
  primaryButtonText: {
    color: DefaultTheme.color.text,
    ...DefaultTheme.fontSize.button,
  },
});

PrimaryButton.propTypes = {
  onPress: PropTypes.func,
  buttonStyle: PropTypes.object,
  textStyle: PropTypes.object,
  text: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  disabled: PropTypes.bool,
};

export default PrimaryButton;
