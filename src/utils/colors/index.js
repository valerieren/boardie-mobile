export const Marigold = '#F3A712';
export const Olivine = '#A8C686';
export const Flame = '#E4572E';
export const AirSuperiorityBlue = '#669BBC';
export const Umber = '#5C5346';

export const Text = '#264653';
export const PersianGreen = '#2A9D8F';
export const Tertiary = '#E9C46A';
export const Secondary = '#F4A261';
export const Primary = '#E76F51';
export const AppBackground = '#F8F8F8';
